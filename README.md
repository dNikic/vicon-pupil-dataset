# vicon-pupil-dataset

Captures of synchronized data from combined Vicon Motion Capture System and Pupil Labs gaze tracking.

Sowftware used to collect data:
https://gitlab.com/vicon-pupil-data-parser/vajkonstrim/tree/linuxport

You may use blender csv tools for post processing of data as keyframes located at:
https://github.com/dnikic/Blender-CSV-import-export