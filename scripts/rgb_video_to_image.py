# This script converts depth videos to iamges and gives a single black frame as
# a starting frame in ordeer to sync it with the RGB video
#Provide a path to the root fodler of the dataset as this example
#/media/HardDisk1TB/pupil_depth_sync

import sys
import os
import cv2
import numpy as np
DATA_PATH = str(sys.argv[1])

for path, subdirs, files in os.walk(DATA_PATH):
        for name in files:
            if name.find('world.mp4') != -1 :#Find video files
                # Create a VideoCapture object
                cap = cv2.VideoCapture(path + "/" + name)
                # Check if camera opened successfully
                if (cap.isOpened() == False):
                  print("Unable to read camera feed")
                # Default resolutions of the frame are obtained.The default resolutions are system dependent.
                # We convert the resolutions from float to integer.
                frame_width = int(cap.get(3))
                frame_height = int(cap.get(4))
                frame_nbr = 0
                try:
                    os.mkdir(str(path)+"/rgb_images/")
                except OSError:
                    print ("Directory allready created at " % DATA_PATH)
                while(True):
                    ret, frame = cap.read()
                    if ret == True:
                        # Write the frame into the file 'output.avi'
                        # out.write(frame)
                        cv2.imwrite(str(path)+"/rgb_images/"+ "Frame_" + str(frame_nbr) + ".png",frame)
                        frame_nbr = frame_nbr + 1
                    else:
                        break
                # When everything done, release the video capture and video write objects
                cap.release()
