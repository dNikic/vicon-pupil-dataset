import pandas as pd
import cv2
import os

output = pd.read_csv('output_clean.csv')
pupil_positions = pd.read_csv('pupil_positions.csv')

# 'Output' je nas csv

output_timestamp = output['pupil1_base_data_0_timestamp']
pupil_timestamp = pupil_positions['pupil_timestamp']
pupil_world_index = pupil_positions['world_index']

elem_lis = []
idx_lis = []

# Trazi nas timestamp u Pupilovim

for i in range(len(output_timestamp)):
    elem = pupil_positions[pupil_positions['pupil_timestamp']==output_timestamp[i]].index.values.astype(int)[0]

    elem_lis.append(elem)

# Daje world index od tog timestampa

for idx in elem_lis:
    idx_lis.append(pupil_world_index[idx])

# Cupa frejmove

world_video = "world_s.mp4" # NE TREBA da bude video iz export foldera

for indeks, frame_n in enumerate(idx_lis):
    try:
        cap = cv2.VideoCapture("world_s.mp4")
        cap.set(1, frame_n)
        ret, frame = cap.read()

        if os.path.exists("frejmovi/frame_"+ str(indeks+1) + ".jpg"):
            print('Skipped overwrite')
            cv2.imwrite("frejmovi/frame_"+ str(indeks+2) + ".jpg", frame)
            indeks += 1
            
        cv2.imwrite("frejmovi/frame_"+ str(indeks+1) + ".jpg", frame)
        print('Saving... ', str(indeks), str(frame_n))
    except:
        print('Fucksy wucksy at ', str(indeks))
        continue
